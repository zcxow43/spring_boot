package com.example.demo.model;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class UseQualifierService2 {
    private AnotherService service;

    public UseQualifierService2(@Qualifier("primaryAnotherService") AnotherService service) {
        this.service = service;
    }

    public void doSomething(){
        System.out.println("choose by constructor:"+service.getPerson());
    }

}