package com.example.demo.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class UseQualifierService {
    @Autowired
    @Qualifier("anotherService")
    private AnotherService service;

    public void doSomething(){
        System.out.println("choose bean name:"+service.getPerson());
    }
}