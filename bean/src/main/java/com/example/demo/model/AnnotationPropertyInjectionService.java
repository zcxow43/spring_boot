package com.example.demo.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnnotationPropertyInjectionService {
    @Autowired
    private SomeService someService;

    @Autowired
    private SomeService2 someService2;


    public void doMyThing(){
        someService.doSomething();
        someService2.doSomething();
    }
}