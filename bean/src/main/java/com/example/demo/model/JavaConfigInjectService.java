package com.example.demo.model;




public class JavaConfigInjectService {
    private AnotherService anotherService;


    public JavaConfigInjectService(AnotherService anotherService) {
        this.anotherService = anotherService;
    }

    public void doMyThing(){
        anotherService.doAnotherThing();
    }
}