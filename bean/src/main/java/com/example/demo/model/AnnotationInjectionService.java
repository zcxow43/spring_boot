package com.example.demo.model;

import org.springframework.stereotype.Service;

@Service
public class AnnotationInjectionService {
    private SomeService someService;

    public AnnotationInjectionService(SomeService someService) {
        this.someService = someService;
    }

    public void doMyThing(){
        someService.doSomething();
    }
}