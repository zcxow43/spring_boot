package com.example.demo.model;

import org.springframework.stereotype.Service;

@Service
public class MixInjectionService {
    private AnotherService anotherService;

    public MixInjectionService(AnotherService anotherService) {
        this.anotherService = anotherService;
    }

    public void doMyThing(){
        anotherService.doAnotherThing();
    }
}