package com.example.demo;

import com.example.demo.model.AnnotationInjectionService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

//@Component
public class GetAllBean implements BeanPostProcessor {

    // pre process
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("----" + beanName + "----");
        System.out.println("----" + beanName.getClass() + "----");
        return bean;
    }

    // after process
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof AnnotationInjectionService){
            System.out.println("++++" + beanName + "++++");
            System.out.println("++++" + beanName.getClass() + "++++");
        }
        return bean;
    }
}