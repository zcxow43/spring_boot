package com.example.demo;

import com.example.demo.model.*;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import javax.annotation.Resource;

@SpringBootApplication
public class DemoApplication {

	@Resource
	private AnnotationPropertyInjectionService annotationPropertyInjectionService;

	@Resource
	private AnnotationSetterInjectionService annotationSetterInjectionService;

	@Resource
	private JavaConfigInjectService javaConfigInjectService;

	@Resource
	private MixInjectionService mixInjectionService;

	@Resource
	private MixInjectionService2 mixInjectionService2;

	@Resource
	private UseQualifierService useQualifierService;

	@Resource
	private UseQualifierService2 useQualifierService2;



	@Bean
	public void beanExecute()
	{
		System.out.println("start beanExecute");
	}

	// show all bean
	@Bean
	public CommandLineRunner run1(ApplicationContext ctx) {
		return args -> {
//			System.out.println("start run1");
//			String[] beanNames = ctx.getBeanDefinitionNames();
//			Arrays.sort(beanNames);
//			for (String beanName : beanNames) {
//				// beanExecute
//				System.out.println(beanName);
//			}
		};
	}

	@Bean
	public CommandLineRunner run2(AnnotationInjectionService annotationInjectionService) {
		return args -> {
			System.out.println("start run2");
			// constructor DI
			annotationInjectionService.doMyThing();
			// Autowired DI
			annotationPropertyInjectionService.doMyThing();
			// Autowired constructor DI
			annotationSetterInjectionService.doMyThing();

			// service declare bean without annotation @Service
			// Use declare bean inside
			javaConfigInjectService.doMyThing();

			// service has annotation @Service
			// Use declare bean inside
			mixInjectionService.doMyThing();

			// service declare bean without annotation @Service
			// Use @component inside
			mixInjectionService2.doMyThing();

			// choose bean name inside
			useQualifierService.doSomething();

			// choose bean name inside by constructor
			useQualifierService2.doSomething();

			System.out.println("end run2");
		};
	}

	@Bean
	public JavaConfigInjectService javaConfigInjectService(AnotherService anotherService){
		return new JavaConfigInjectService(anotherService);
	}

	@Bean
	@Primary
	public AnotherService primaryAnotherService(){
		return new AnotherService("pre-define ");
	}

	// primary first use
	@Bean
	public AnotherService anotherService(){
		return new AnotherService("no use");
	}

	@Bean
	public MixInjectionService2 mixInjectionService2(SomeService someService){
		return new MixInjectionService2(someService);
	}
	//*/

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}