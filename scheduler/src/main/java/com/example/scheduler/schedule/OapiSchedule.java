package com.example.scheduler.schedule;

//import com.oapi.service.oapi.OapiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.SimpleDateFormat;
import java.util.Date;


public class OapiSchedule
{
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");


//	private static OapiService oapiService;

//	public OapiSchedule(OapiService oapiService)
//	{
//		this.oapiService  = oapiService;
//	}

	@Scheduled(cron = "0/10 * * * * ?")
	public void OapiScheduleCron() throws Exception
	{
		logger.info("refill oapi count:{}", dateFormat.format(new Date()));
//		oapiService.resetOapiCount();
	}
}