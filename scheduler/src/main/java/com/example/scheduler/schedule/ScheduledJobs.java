package com.example.scheduler.schedule;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;


@Configuration
@EnableScheduling
public class ScheduledJobs {

	@Bean
	@ConditionalOnProperty(value = "jobs.enabled", matchIfMissing = true, havingValue = "true")
	public OapiSchedule scheduledJob() {
		return new OapiSchedule();
//		return new OapiSchedule(oapiService);
	}
}