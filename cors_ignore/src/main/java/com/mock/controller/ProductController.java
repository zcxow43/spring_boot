package com.mock.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ProductController
{
	@RequestMapping(value = "/createProduct",
			method = { RequestMethod.POST, RequestMethod.PUT },
			produces = { "application/json" })
	public String createProduct(@RequestBody String payload) throws Exception
	{
		return "{\"result\":{\"key\":\"createProduct\"}}";
	}

	@RequestMapping(value = "/updateProduct",
			method = { RequestMethod.POST, RequestMethod.PUT },
			produces = { "application/json" })
	public String updateProduct(@RequestBody String payload) throws Exception
	{
		return "{\"result\":{\"key\":\"updateProduct\"}}";
	}
}