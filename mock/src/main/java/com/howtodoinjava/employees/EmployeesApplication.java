package com.howtodoinjava.employees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeesApplication {

	// https://howtodoinjava.com/spring-boot2/testing/spring-boot-mockito-junit-example/
	public static void main(String[] args) {
		SpringApplication.run(EmployeesApplication.class, args);
	}
}