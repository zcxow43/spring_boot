package com.example.httpexample;

import com.example.httpexample.model.GetProductRequest;
import com.example.httpexample.model.HttpRequestBase;
import com.google.gson.Gson;
import org.apache.http.HttpHost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URL;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


@SpringBootApplication
@Configuration
public class HttpexampleApplication {

	protected final Logger log = LoggerFactory.getLogger(getClass());

	public static void main(String[] args) {
		SpringApplication.run(HttpexampleApplication.class, args);
	}

	@Bean
	public void sendRequst() throws Exception
	{
		String method = "GET";

		/* replace url */
		String uri = "https://mmstest.hkmpcl.com.hk/mmsAdmin/oapi/api/product/details";
		URL url = new URL(uri);

		/* replace body */
		String body = createBody();
		HttpRequest requestOut = new HttpRequestBase(url.toURI(), body, method);

		log.info("set header");
		requestOut.addHeader("content-type", "application/json");

		/* replace header */
		requestOut.addHeader("x-auth-token", "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJzaG9hbHRlciIsIm5hbWUiOiJzaG9hbHRlciIsImlhdCI6MTY1MDg3NDgyMCwieC1hcGkta2V5IjoiNjRiN2RkNjgtMGI4Yi00OTRmLTg0MDEtYWFmZTczN2NhMjY4In0.QImzdnXmJFqIulfcdlU6qlgShg6m11nJ2t8Rcme7o7UvUdX2MBSNVfGPojvBt_rO8UaK3VVOhrcRPngiDs5H42auCDCVER72eAQz9hzBVvr5xixRxiSsKOHlIuQ29yxGGnQUr47nIUWgcOPQ9-i81OhLd5ntGbC5wBdm9zuAt1wab1nZ2j2nXEt4sNusD8KvGvuhReSRPGvzl5bG0d3g5MSO6CvzG9yOk3NAykaxUE8eIfBksZQ56DHGCXLTVWl072Jxupy23KTwbNKCsra8X4ZkK149XCmBPed3FoydzAC4H4J11wczqmdcrzSHWPqRHk_TuaxJawXBEkUR-v0sYA");
		requestOut.addHeader("storeCode", "H8220001");
		requestOut.addHeader("platformCode", "HKTV");
		requestOut.addHeader("businessType", "eCommerce");

		log.info("send request, url:" + url);
		HttpResponse response;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		if (url.getPort() != -1)
		{
			response = httpClient.execute(new HttpHost(url.getHost(), url.getPort()), requestOut);
		}
		else
		{
			response = httpClient.execute(new HttpHost(url.getHost()), requestOut);
		}

		String result = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
		httpClient.close();
		log.info(result);
	}

	String createBody()
	{
		List<GetProductRequest> list = new ArrayList<>();
		GetProductRequest obj = new GetProductRequest();
		obj.setSkuCode("H8220001_S_a1");
		list.add(obj);
		return new Gson().toJson(list);
	}
}