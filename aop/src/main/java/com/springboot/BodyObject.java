package com.springboot;

public class BodyObject
{
	private String a1;
	private String a2;
	private Integer a3;

	public String getA1()
	{
		return a1;
	}

	public void setA1(String a1)
	{
		this.a1 = a1;
	}

	public String getA2()
	{
		return a2;
	}

	public void setA2(String a2)
	{
		this.a2 = a2;
	}

	public Integer getA3()
	{
		return a3;
	}

	public void setA3(Integer a3)
	{
		this.a3 = a3;
	}
}