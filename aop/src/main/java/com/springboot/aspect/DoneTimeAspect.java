package com.springboot.aspect;

import com.google.gson.Gson;
import com.springboot.BodyObject;
import com.springboot.annotation.InjectMethod;
import com.springboot.annotation.DoneTime;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Aspect
@Component
public class DoneTimeAspect {
    @Around("@annotation(doneTime)")
    public Object around(ProceedingJoinPoint joinPoint, DoneTime doneTime) throws Throwable {
        System.out.println("方法开始时间是:"+new Date());
        Object o = joinPoint.proceed();
        System.out.println("方法结束时间是:"+new Date()) ;
        return o;
    }

    @Around("@annotation(param)")
    public Object addAround(ProceedingJoinPoint joinPoint, InjectMethod param) throws Throwable {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String header = request.getHeader("sessionGuid");

        Gson gson = new Gson();
        List<Object> bodyList = Arrays.asList(joinPoint.getArgs());
        Object body = bodyList.get(0);
        System.out.println("Around【body】："+ gson.toJson(body));

        Integer sum = 0;
        if (body instanceof Collection)
        {
            sum = new ArrayList<>((Collection<?>) body).size();
        }
        System.out.println("Around【sum】："+ sum);

        System.out.println("Around first:"+param.param1());
        Object o = joinPoint.proceed();
        System.out.println("Around after:"+param.param2());

        BodyObject a1 = new BodyObject();
        a1.setA1("1");
        a1.setA2("2");

//        return a1;
        return joinPoint.proceed();
    }

//    @Before("@annotation(param)")
//    public Object addBefore(JoinPoint joinPoint, InjectMethod param){
//
//        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//        String header = request.getHeader("sessionGuid");
//
//        Gson gson = new Gson();
//        List<Object> bodyList = Arrays.asList(joinPoint.getArgs());
//        Object body = bodyList.get(0);
//        System.out.println("【body】："+ gson.toJson(body));
//
//        Integer sum = 0;
//        if (body instanceof Collection)
//        {
//            sum = new ArrayList<>((Collection<?>) body).size();
//        }
//        System.out.println("【sum】："+ sum);
//
//        System.out.println("first:"+param.param1());
//
//        BodyObject a1 = new BodyObject();
//        a1.setA1("1");
//        a1.setA2("2");
//
//        return a1;
//    }
}