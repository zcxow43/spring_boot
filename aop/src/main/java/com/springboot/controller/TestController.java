package com.springboot.controller;

import com.springboot.BodyObject;
import com.springboot.annotation.DoneTime;
import com.springboot.annotation.InjectMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class TestController {
    @GetMapping("/test1")
    @DoneTime(param = "TestController")
    public String test1(){
        System.out.println("方法1执行");
        return "hello springboot2";
    }

    @GetMapping("/test2")
    public String test2(){
        System.out.println("方法2执行");
        return "hello springboot2";
    }

    @GetMapping("/test5")
    @InjectMethod(param1 = "a1", param2 = "a2")
    public BodyObject test5(@RequestBody List<BodyObject> list){
        System.out.println("方法5执行");
        BodyObject b = new BodyObject();
        b.setA1("3");
        b.setA2("4");
        return b;
    }

}