package com.example.form;

import javax.validation.constraints.*;

public class User {

//	private final String errorMsg = "The name '${validatedValue}' must be at least {min}" + " characters long. Length found : ${validatedValue.length()}";

	@Size(max = 10, message = "name must less than {max}")
	@NotEmpty(message = "name can not be empty")
	private String name;

	@AssertTrue(message = "working must be true")
	private boolean working;

	@Size(max = 10, message = "aboutMe must less than {max}")
	private String aboutMe;

	@Min(value = 18, message = "Age should not be less than 18")
//	@Max(value = 20, message = errorMsg)
	private int age;


	@CheckDateFormat(pattern = "yyyy-MM-dd")
	private String dateStr;

	@Email(message = "email format error, email:${validatedValue}")
	private String email;

	@Pattern(regexp = "09\\d{2}-\\d{3}-\\d{3}",message = "phone error, phone:${validatedValue}")
	private String phone;

	// standard setters and getters

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public boolean isWorking()
	{
		return working;
	}

	public void setWorking(boolean working)
	{
		this.working = working;
	}

	public String getAboutMe()
	{
		return aboutMe;
	}

	public void setAboutMe(String aboutMe)
	{
		this.aboutMe = aboutMe;
	}

	public int getAge()
	{
		return age;
	}

	public void setAge(int age)
	{
		this.age = age;
	}

	public String getDateStr()
	{
		return dateStr;
	}

	public void setDateStr(String dateStr)
	{
		this.dateStr = dateStr;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}