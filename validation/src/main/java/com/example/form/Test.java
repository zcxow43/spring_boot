package com.example.form;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;


public class Test
{
	public void StartMain()
	{
		Logger LOG = LoggerFactory.getLogger(getClass());
		User entity = new User();

		entity.setName("11111111111111111111111");
		entity.setDateStr("2020-01");
		entity.setEmail("zcx");
		entity.setPhone("0800123456");

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<User>> violations = validator.validate(entity);
		for (ConstraintViolation<User> violation : violations)
		{
			LOG.error(violation.getMessage());
		}
	}
}