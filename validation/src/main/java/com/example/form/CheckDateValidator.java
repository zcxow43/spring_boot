package com.example.form;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.SimpleDateFormat;
import java.util.Date;


public class CheckDateValidator implements ConstraintValidator<CheckDateFormat, String>
{
	private String pattern;

	@Override
	public void initialize(CheckDateFormat constraintAnnotation) {
		this.pattern = constraintAnnotation.pattern();
	}

	@Override
	public boolean isValid(String object, ConstraintValidatorContext constraintContext) {
		if ( object == null ) {
			return true;
		}

		try {
			new SimpleDateFormat(pattern).parse(object);
			return true;
		} catch (Exception e) {
			return false;
		}

	}
}