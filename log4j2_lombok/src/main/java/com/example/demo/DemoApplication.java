package com.example.demo;

import lombok.extern.log4j.Log4j2;
//import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@Slf4j
@Log4j2
@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		log.info("hello world.");
	}

}