package com.springboot.repsitory;

import com.springboot.entity.City;
import com.springboot.entity.House;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HouseRepository extends JpaRepository<House,Long> {

}