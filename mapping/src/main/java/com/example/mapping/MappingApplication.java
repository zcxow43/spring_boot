package com.example.mapping;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class MappingApplication {

	public static void main(String[] args) {
		SpringApplication.run(MappingApplication.class, args);
	}

	@RequestMapping(value = "/a1", method = {RequestMethod.POST}, produces = { "application/json" })
	public void a1()
	{
		Person person = new Person();
		person.setAge(10);
		person.setName("hever");
		person.setNickName("ko");


		Robot robot = PersonMapper.MAPPER.clone(person);

		int aa=0;
	}

	@RequestMapping(value = "/test", method = {RequestMethod.POST}, produces = { "application/json" })
	public void test(@RequestBody Person person)
	{
	}

}