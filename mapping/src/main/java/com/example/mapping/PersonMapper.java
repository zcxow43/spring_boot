package com.example.mapping;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.control.DeepClone;
import org.mapstruct.factory.Mappers;

@Mapper(mappingControl = DeepClone.class)
public interface PersonMapper {

    PersonMapper MAPPER = Mappers.getMapper( PersonMapper.class );

    @Mapping(source = "name", target = "serialNumber")
    Robot clone(Person person);
}