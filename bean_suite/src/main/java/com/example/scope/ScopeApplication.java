package com.example.scope;

import com.example.scope.service.PrototypeService;
import com.example.scope.service.SingletonService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@SpringBootApplication
public class ScopeApplication {



	public static void main(String[] args) {
		SpringApplication.run(ScopeApplication.class, args);
	}

}