package com.example.scope.enable;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AConfig {

    @Bean
    public String name(){
        return "Hever";
    }

    @Bean
    public Integer number(){
        return 123;
    }

}