package com.example.scope.service;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Service
@Lazy
// lazy can save resources if this service not be inject
public class LifeService {
    public LifeService()
    {
        System.out.println("LifeService construct");
    }

    public void func()
    {
        System.out.println("func");
    }

    @PreDestroy
    public void preDestroy()
    {
        System.out.println("LifeService destroy before");
    }

    @PostConstruct
    public void postConstract()
    {
        System.out.println("LifeService constructor after");
    }
}