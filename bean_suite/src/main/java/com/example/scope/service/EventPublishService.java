package com.example.scope.service;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class EventPublishService {
    ApplicationEventPublisher publisher;

    public EventPublishService(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    public void publish(){
        System.out.println("EventPublishService run");
        publisher.publishEvent(new MessageEvent("EventPublishService complete"));
    }
}