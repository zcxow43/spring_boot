package com.example.scope.service;

import org.springframework.context.event.*;
import org.springframework.stereotype.Component;

@Component
public class EventListenerService2 {

    @EventListener
    public void eventListener(MessageEvent event){
        System.out.println("eventListener：" + event.getMessage());
    }

    @EventListener
    public void contextRefreshedEventListener(ContextRefreshedEvent event){
        System.out.println("contextRefreshedEventListener：" + event.getSource());
    }

    @EventListener
    public void contextStartedEventListener(ContextStartedEvent event){
        System.out.println("contextStartedEventListener：" + event.getSource());
    }

    @EventListener
    public void contextStoppedEventListener(ContextStoppedEvent event){
        System.out.println("contextStoppedEventListener：" + event.getSource());
    }

    @EventListener
    public void contextClosedEventListener(ContextClosedEvent event){
        System.out.println("contextClosedEventListener：" + event.getSource());
    }

}