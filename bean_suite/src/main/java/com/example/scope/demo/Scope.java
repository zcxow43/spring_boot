package com.example.scope.demo;

import com.example.scope.service.PrototypeService;
import com.example.scope.service.SingletonService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

//@Configuration
public class Scope {
    @Resource
    SingletonService single1;

    @Resource
    SingletonService single2;

    @Resource
    PrototypeService prototype1;

    @Resource
    PrototypeService prototype2;

    @Bean
    public void startScope()
    {
        // single bean use same object
        single1.name="a2";
        System.out.println("single:"+single1.name.equals(single2.name));

        // single bean use different object
        prototype2.name="a2";
        System.out.println("prototype:"+prototype1.name.equals(prototype2.name));
    }
}