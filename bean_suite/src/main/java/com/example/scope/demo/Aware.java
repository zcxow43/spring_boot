package com.example.scope.demo;

import com.example.scope.service.AwareSpringService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class Aware {

    // when component needs get bean info
    @Bean
    CommandLineRunner awareClr(AwareSpringService awareSpringService){
        return args -> awareSpringService.doSomething();
    }
}