package com.example.scope.demo;

import com.example.scope.enable.EnableA;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
@EnableA
public class Enable {

    // note method name same with parameter name
    // inject AConfig.name
    @Bean
    CommandLineRunner a1(String name){
        return args -> System.out.println(name);
    }

    // inject AConfig.number
    @Bean
    CommandLineRunner a2(Integer number){
        return args -> System.out.println(number);
    }


}