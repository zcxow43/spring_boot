package com.example.scope.demo;

import com.example.scope.service.LifeService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

//@Component
public class GetAllBean implements BeanPostProcessor {

    // pre process
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("----" + beanName + "----");
        System.out.println("----" + beanName.getClass() + "----");
        return bean;
    }

    // after process
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof LifeService){
            System.out.println("++++" + beanName + "++++");
            System.out.println("++++" + beanName.getClass() + "++++");
        }
        return bean;
    }
}