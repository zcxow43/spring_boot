package com.example.scope.demo;

import com.example.scope.service.EventPublishService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
public class BeanMessager {
    @Resource
    EventPublishService eventPublishService;

    @Bean
    CommandLineRunner messageClr(){
        return args->{
            eventPublishService.publish();
        };
    }
}