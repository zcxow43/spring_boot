package com.example.httpexample;

import com.example.httpexample.model.GetProductRequest;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


@SpringBootApplication
@Configuration
public class HttpexampleApplication {

	protected final Logger log = LoggerFactory.getLogger(getClass());

	public static void main(String[] args) {
		SpringApplication.run(HttpexampleApplication.class, args);
	}

	@Bean
	public void sendRequst() throws Exception
	{
		/* replace url */
//		String url = "https://mms-api.shoalter.com/mmsAdmin/oapi/api/product/details";
		String url = "https://mms-api.shoalter.com/mmsAdmin/oapi/api/store/details";
		String method = "GET";
		RequestBuilder requestOut = RequestBuilder
				.create(method)
				.setUri(url);

		/* replace header */
		log.info("set header");
		requestOut.addHeader("x-auth-token", "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJzaG9hbHRlciIsIm5hbWUiOiJzaG9hbHRlciIsImlhdCI6MTY1MjA3Nzc4NywieC1hcGkta2V5IjoiNjRiN2RkNjgtMGI4Yi00OTRmLTg0MDEtYWFmZTczN2NhMjY4In0.fsM5WUsPjKJIkjvOCkMMacU3bjqPm0RWz97vSfJkPJ5d1G-cwGA3a7VeRjzMkhr5JgPaFR4bozzLrTJnyOM3u5h_GlsrdnMun4oHFb5ZEklMVAr25pqUQNgL1vxhHHi-gLGOuFzjhnEcuLCpjSLm6soAkeRqEanA0vNas0gDnL94f3WpeC5oqyXqmafzv-We0z3HexgImU7Jm6-1hZ7NJ8ojn9wFqx-gngcoETd2fxdPbYWTDIJxxTfDidEd2ss7vmHBiDf9HmvzRagGi00QV0LbFehk51f7hHhUMIHsj8P4J7_h8iIC3lisAA4MO0q_8g81SE5kk4m2fewj8Ezctw");
		requestOut.addHeader("storeCode", "H8220001");
		requestOut.addHeader("platformCode", "HKTV");
		requestOut.addHeader("businessType", "eCommerce");

		/* replace body */
		log.info("set body");
		String body = createBody();
		requestOut.setEntity(new StringEntity(body, ContentType.APPLICATION_JSON));

		log.info("send request, url:" + url);
		HttpResponse response;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		response = httpClient.execute(requestOut.build());
		String result = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
		httpClient.close();
		log.info(result);
	}

	String createBody()
	{
		List<GetProductRequest> list = new ArrayList<>();
		GetProductRequest obj = new GetProductRequest();
		obj.setSkuCode("H8220001_S_a1");
		list.add(obj);
		return new Gson().toJson(list);
	}
}