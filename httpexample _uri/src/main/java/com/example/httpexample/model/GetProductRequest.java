package com.example.httpexample.model;


public class GetProductRequest
{
	private String skuCode;

	public String getSkuCode()
	{
		return skuCode;
	}

	public void setSkuCode(String skuCode)
	{
		this.skuCode = skuCode;
	}
}