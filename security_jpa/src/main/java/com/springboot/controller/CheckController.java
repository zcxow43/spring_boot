package com.springboot.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//@Secured({ "USER" })

@RestController
public class CheckController {

    @RequestMapping("/check")
    public String select(){
        return "check";
    }


}