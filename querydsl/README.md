
parent version has big affect framework work 
```aidl
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.2.0.RELEASE</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
```

### 1. mvn compile
![img_1.png](img_1.png)

### 2. set generated folder into project package
![img.png](img.png)