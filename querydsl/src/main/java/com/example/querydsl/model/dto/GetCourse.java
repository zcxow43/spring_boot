package com.example.querydsl.model.dto;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class GetCourse
{
	private Long id;
	private String name;
	private Boolean sex;

	public GetCourse(Long id, String name, Boolean sex)
	{
		this.id = id;
		this.name = name;
		this.sex = sex;
	}
}