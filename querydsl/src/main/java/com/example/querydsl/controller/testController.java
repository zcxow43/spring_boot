package com.example.querydsl.controller;

import com.example.querydsl.model.entity.Course;
import com.example.querydsl.model.entity.QCourse;
import com.example.querydsl.model.entity.QLecturer;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class testController
{

	@Autowired
	private JPAQueryFactory queryFactory;

	@RequestMapping(value = "/aaa", method = { RequestMethod.GET })
	public String aaa()
	{
		QCourse qCourse = QCourse.course;
		QLecturer qLecturer = QLecturer.lecturer;
		// 这里包含了组装查询条件和执行查询的逻辑，组装好条件后记得执行fetch()
		List<Course> courses=queryFactory.select(qCourse)
				.from(qCourse)
				.leftJoin(qLecturer)
				.on(qCourse.lecturerId.eq(qLecturer.id))
				.where(qLecturer.name.eq("Tom"))
				.fetch();
		System.out.println(courses);

		return "pass";
	}

}