package com.example.security;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @RequestMapping("/aaa")
    public String aaa(){
        return "aaa";
    }

    @RequestMapping("/select")
    public String select(){
        return "select";
    }

    @RequestMapping("/check")
    public String check(){
        return "check";
    }

    @RequestMapping("/css")
    public String css(){
        return "css";
    }

}
