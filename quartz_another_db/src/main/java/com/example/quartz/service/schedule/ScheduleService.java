package com.example.quartz.service.schedule;

import com.example.quartz.config.quartz.QuartzConfig;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class ScheduleService
{
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Resource
	private QuartzConfig quartzConfig;

	public void addJob(Class cls, String jobName, String triggerName, String expression) throws Exception
	{
		try
		{
			// job
			JobDetail jobDetail = JobBuilder.newJob(cls).withIdentity(jobName).build();

			// trigger
			CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(expression);
			CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(triggerName)
					.withSchedule(scheduleBuilder).build();

			// scheduler
			Scheduler scheduler = quartzConfig.scheduler();
			scheduler.start();
			if (scheduler.checkExists(jobDetail.getKey()))
			{
				// delete old in db one
				scheduler.deleteJob(jobDetail.getKey());
			}
			scheduler.scheduleJob(jobDetail, trigger);
		}
		catch (SchedulerException e)
		{
			log.error("addJob error:" + e.getMessage());
			throw new Exception("setSchedule error:" + e.getMessage());
		}
	}
}