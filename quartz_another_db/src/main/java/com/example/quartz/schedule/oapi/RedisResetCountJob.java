package com.example.quartz.schedule.oapi;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;


@Service
public class RedisResetCountJob implements Job {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

//	@Resource
//	private OapiService oapiService;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		log.info("refill oapi count:{}", dateFormat.format(new Date()));
//		try
//		{
//			oapiService.resetOapiCount();
//		}
//		catch (Exception e)
//		{
//			log.error("RedisResetCountJob error:" + e.getMessage());
//		}
	}
}