package com.example.quartz.schedule;

import com.example.quartz.schedule.oapi.RedisResetCountJob;
import com.example.quartz.service.schedule.ScheduleService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;


@Configuration
public class JobManager
{
	@Value("${oapi.expression:0/5 * * * * ?}")
	String oapiExpression;

	@Resource
	ScheduleService scheduleService;

	@Bean
	public void run() throws Exception
	{
		scheduleService.addJob(RedisResetCountJob.class, "RedisResetCountJob","RedisResetCountTrigger", oapiExpression);
	}
}