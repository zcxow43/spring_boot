package com.example.thread.thread;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;


public class ForkJoinCalculator implements Calculator {
	private ForkJoinPool pool;

	private static class SumTask extends RecursiveTask<Long>
	{
		private long[] numbers;
		private int from;
		private int to;

		public SumTask(long[] numbers, int from, int to) {
			this.numbers = numbers;
			this.from = from;
			this.to = to;
		}

		@Override
		protected Long compute() {
			// 當需要計算的數字小於6時，直接計算結果
			if (to - from < 6) {
				long total = 0;
				for (int i = from; i <= to; i++) {
					total += numbers[i];
				}
				return total;
				// 否則，把任務一分為二，遞迴計算
			} else {
				int middle = (from + to) / 2;
				SumTask taskLeft = new SumTask(numbers, from, middle);
				SumTask taskRight = new SumTask(numbers, middle+1, to);
				taskLeft.fork();
				taskRight.fork();
				return taskLeft.join() + taskRight.join();
			}
		}
	}

	public ForkJoinCalculator() {
		// 也可以使用公用的 ForkJoinPool：
//		pool = ForkJoinPool.commonPool();
		pool = new ForkJoinPool();

	}

	@Override
	public long sumUp(long[] numbers) {

		long aa = pool.invoke(new SumTask(numbers, 0, numbers.length-1));
		System.out.println(pool.getPoolSize());
		return aa;
	}
}