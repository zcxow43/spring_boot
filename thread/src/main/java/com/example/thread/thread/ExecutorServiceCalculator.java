package com.example.thread.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class ExecutorServiceCalculator implements Calculator {
	private int parallism;
	private ExecutorService pool;

	public ExecutorServiceCalculator() {
		parallism = Runtime.getRuntime().availableProcessors(); // CPU的核心數
		pool = Executors.newFixedThreadPool(parallism);
	}

	private static class SumTask implements Callable<Long>
	{
		private long[] numbers;
		private int from;
		private int to;

		public SumTask(long[] numbers, int from, int to) {
			this.numbers = numbers;
			this.from = from;
			this.to = to;
		}

		@Override
		public Long call() throws Exception {
			long total = 0;
			for (int i = from; i <= to; i++) {
				total += numbers[i];
			}
			return total;
		}
	}

	@Override
	public long sumUp(long[] numbers) {
		List<Future<Long>> results = new ArrayList<>();

		// 把任務分解為 n 份，交給 n 個執行緒處理
		int part = numbers.length / parallism;
		for (int i = 0; i < parallism; i++) {
			int from = i * part;
			int to = (i == parallism - 1) ? numbers.length - 1 : (i + 1) * part - 1;
			results.add(pool.submit(new SumTask(numbers, from, to)));
		}

		// 把每個執行緒的結果相加，得到最終結果
		long total = 0L;
		for (Future<Long> f : results) {
			try {
				total += f.get();
			} catch (Exception ignore) {}
		}

		pool.shutdownNow();

		return total;
	}
}