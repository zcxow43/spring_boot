package com.example.thread.thread;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.stream.LongStream;


public class Main
{
	public static void main(String[] args) {
		long[] numbers = LongStream.rangeClosed(1, 100000000).toArray();
//		Calculator calculator = new ForLoopCalculator();
		Calculator calculator = new ExecutorServiceCalculator();
//		Calculator calculator = new ForkJoinCalculator();
		long start = System.currentTimeMillis();
		System.out.println(calculator.sumUp(numbers)); // 列印結果500500
		long end   = System.currentTimeMillis();

		NumberFormat formatter = new DecimalFormat("#0.00000");
		System.out.print("Execution time is " + formatter.format((end - start) / 1000d) + " seconds");
	}

}