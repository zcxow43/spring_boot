
we override clone method for swallow copy

```java
@Override
protected Object clone() throws CloneNotSupportedException
{
   return super.clone();
}
```

if target object has structural child class, we clone and set for deep copy
```java
@Override
public Object clone() throws CloneNotSupportedException
{
    Store store = (Store)super.clone();
    store.setUser((User)this.getUser().clone());
    return store;
}
```

### reference
https://howtodoinjava.com/java/cloning/a-guide-to-object-cloning-in-java/