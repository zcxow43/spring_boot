package com.example.demo;

public class User implements Cloneable
{
	private String name;

	@Override
	protected Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}

	@Override
	public String toString()
	{
		return this.getName();
	}

	public User(){

	}

	public User(String name){
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

}