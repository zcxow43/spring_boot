package com.example.demo;

import java.util.ArrayList;
import java.util.List;

public class Store implements Cloneable
{
	private String s1;
	private String s2;
	private User user;
	private User2 user2;
	private List<User> userList;

	public List<String> getSsList()
	{
		return ssList;
	}

	public void setSsList(List<String> ssList)
	{
		this.ssList = ssList;
	}

	private List<String> ssList;

	@Override
	public Object clone() throws CloneNotSupportedException {
		Store store = (Store)super.clone();
		store.setUser((User)this.getUser().clone());
		store.setUserList(new ArrayList<>(this.getUserList()));
		store.setSsList(new ArrayList<>(this.getSsList()));
		return store;
	}

	public String getS1()
	{
		return s1;
	}

	public void setS1(String s1)
	{
		this.s1 = s1;
	}

	public String getS2()
	{
		return s2;
	}

	public void setS2(String s2)
	{
		this.s2 = s2;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public User2 getUser2()
	{
		return user2;
	}

	public void setUser2(User2 user2)
	{
		this.user2 = user2;
	}

	public List<User> getUserList()
	{
		return userList;
	}

	public void setUserList(List<User> userList)
	{
		this.userList = userList;
	}

}