package com.example.demo;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@Configuration
@SpringBootApplication
public class DemoApplication {

	private Logger log = LoggerFactory.getLogger(DemoApplication.class);

	@Bean
	public void start() throws CloneNotSupportedException
	{
		Store store = new Store();
		store.setS1("s1");

		User user = new User();
		user.setName("u1");
		store.setUser(user);

		User2 user2 = new User2();
		user2.setName("u2");
		store.setUser2(user2);

		List<User> userList = new ArrayList(){{
			add(new User("list1"));
			add(new User("list2"));
		}};
		store.setUserList(userList);

		List<String> ssList = Arrays.asList("1","2");
		store.setSsList(ssList);

		Store storeClone = (Store) store.clone();
		storeClone.setS1("c1");
		storeClone.getUser().setName("clone1");	// success
		storeClone.getUser2().setName("clone2");	// fail clone
		storeClone.getUserList().add(new User("clone1"));	// success
		storeClone.getSsList().add("clone_ss");

		log.info("s1 origin:{}, after:{}", store.getS1(), storeClone.getS1());
		log.info("user origin:{}, after:{}", store.getUser().getName(), storeClone.getUser().getName());
		log.info("user2 origin:{}, after:{}", store.getUser2().getName(), storeClone.getUser2().getName());
		log.info("userlist origin:{}, after:{}", store.getUserList(), storeClone.getUserList());
		log.info("ssList origin:{}, after:{}", store.getSsList(), storeClone.getSsList());

		// json deep clone
		Gson gson = new Gson();
		Store store2 = gson.fromJson(gson.toJson(store), Store.class);
		store2.setS1("s2");
		log.info("s1 origin:{}, after:{}", store.getS1(), store2.getS1());
	}

	@Bean
	public void start2() throws CloneNotSupportedException
	{

	}


	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}