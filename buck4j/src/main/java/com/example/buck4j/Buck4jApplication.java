package com.example.buck4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Buck4jApplication {

	public static void main(String[] args) {
		SpringApplication.run(Buck4jApplication.class, args);
	}

}
