package com.example.buck4j;

import com.example.buck4j.annotation.OpenApiAspectPoint2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class OpenApiAspect
{
//    private final Map<String, Bucket> bucketDic = new HashMap<>();
//
//    public OpenApiAspect()
//    {
//        createBucket("aaa",1);
//        createBucket("bbb",3);
//    }
//
//    private void createBucket(String key, Integer amount)
//    {
//        Bandwidth limit = Bandwidth.classic(amount, Refill.intervally(amount, Duration.ofSeconds(5)));
//        Bucket bucket = Bucket4j.builder()
//              .addLimit(limit)
//              .build();
//        bucketDic.put(key, bucket);
//    }


    @Around("@annotation(param)")
    public Object addAround(ProceedingJoinPoint joinPoint, OpenApiAspectPoint2 param) throws Throwable {
        System.out.println("aspect");

//        Bucket bucket = bucketDic.get(param.key());
//        if (bucket.tryConsume(1)) {
//           return joinPoint.proceed();
//        }


//        System.out.println("Around first:"+param.param1());
//        Object o = joinPoint.proceed();
//        System.out.println("Around after:"+param.param2());
//
//        BodyObject a1 = new BodyObject();
//        a1.setA1("1");
//        a1.setA2("2");

        return joinPoint.proceed();
    }

}