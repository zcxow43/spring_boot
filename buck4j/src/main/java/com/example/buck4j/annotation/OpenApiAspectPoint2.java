package com.example.buck4j.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.RUNTIME)
public @interface OpenApiAspectPoint2
{
    String key() default "";
}