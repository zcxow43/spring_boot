package com.example.buck4j;

import com.example.buck4j.annotation.OpenApiAspectPoint2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class TestController
{
//	private final Bucket bucket;
//	public TestController()
//	{
//		Bandwidth limit = Bandwidth.classic(1, Refill.intervally(1, Duration.ofSeconds(5)));
//		this.bucket = Bucket4j.builder()
//				.addLimit(limit)
//				.build();
//	}

	@GetMapping("/aaa")
	@OpenApiAspectPoint(key = "aaa")
	private String aaa()
	{
		System.out.println("aaa");
		return "aaa";
	}

	@OpenApiAspectPoint2(key = "bbb")
	@RequestMapping("bbb")
	private String bbb()
	{
//		if (bucket.tryConsume(1)) {
//			return "bbb";
//		}
		return "bbb";
	}
}