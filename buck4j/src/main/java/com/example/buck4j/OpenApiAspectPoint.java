package com.example.buck4j;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.RUNTIME)
public @interface OpenApiAspectPoint
{
    String key() default "";
}