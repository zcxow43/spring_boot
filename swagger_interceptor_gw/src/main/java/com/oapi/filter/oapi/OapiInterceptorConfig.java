package com.oapi.filter.oapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;



@Configuration
public class OapiInterceptorConfig implements WebMvcConfigurer
{
	@Autowired
	private OapiInterceptor openApiInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(openApiInterceptor)
				.addPathPatterns("/**/oapi/api/**");
	}
}