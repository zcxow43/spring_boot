package com.oapi.filter.oapi;

import com.oapi.model.dto.oapi.OpenApiResponse;
import com.oapi.service.oapi.OapiService;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class OapiInterceptor implements HandlerInterceptor {

	@Resource
	OapiService oapiService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
	{
		try
		{
			OpenApiResponse responseObj = oapiService.forward(request);
			oapiService.setOapiResponse(response, responseObj);
		}
		catch (Exception ex)
		{
			OpenApiResponse responseObj = new OpenApiResponse();
			responseObj.setCode(OpenApiResponse.Code.error);
			responseObj.setMessage(ex.getMessage());
			oapiService.setOapiResponse(response, responseObj);
		}

		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	}
}