package com.oapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@SpringBootApplication
public class MmsOapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmsOapiApplication.class, args);
	}

}