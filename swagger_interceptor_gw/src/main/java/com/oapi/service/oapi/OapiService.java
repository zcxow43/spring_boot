package com.oapi.service.oapi;

import com.oapi.model.dto.oapi.OpenApiResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;


public interface OapiService
{
	<T> T forward(HttpServletRequest request, T responseSample) throws IOException, URISyntaxException;

	OpenApiResponse forward(HttpServletRequest request) throws Exception;

	void setOapiResponse(HttpServletResponse response, OpenApiResponse responseObj) throws Exception;
}