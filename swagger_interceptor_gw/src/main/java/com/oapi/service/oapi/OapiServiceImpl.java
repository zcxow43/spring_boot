package com.oapi.service.oapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.oapi.filter.oapi.OapiHttpRequestWrapper;
import com.oapi.model.dto.oapi.OpenApiResponse;
import com.oapi.model.dto.common.HttpRequestBase;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service("HttpService")
public class OapiServiceImpl implements OapiService
{
	protected final Logger LOG = LoggerFactory.getLogger(getClass());

	@Value("${mms.url}")
	String mmsUrl;

	final List<String> headerBan = Arrays.asList("content-length");

	@Override
	public <T> T forward(HttpServletRequest request, T responseSample) throws IOException, URISyntaxException
	{
		LOG.info("start forward, uri:" + request.getRequestURI());
		String method = request.getMethod();
		String urlStr = mmsUrl + request.getRequestURI();
		OapiHttpRequestWrapper wrapper = new OapiHttpRequestWrapper(request);
		String body = wrapper.getBodyInStringFormat();
		URL url = new URL(urlStr);
		HttpRequest requestOut = new HttpRequestBase(url.toURI(), body, method);

		LOG.info("set header");
		Enumeration<String> headerNames = request.getHeaderNames();
		if (headerNames != null)
		{
			while (headerNames.hasMoreElements())
			{
				String key = headerNames.nextElement();
				String value = request.getHeader(key);
				if (!headerBan.contains(key))
				{
					requestOut.addHeader(key, value);
				}
			}
		}

		LOG.info("send request, url:{}", url.getHost());
		HttpResponse response;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		if (url.getPort() != 0)
		{
			response = httpClient.execute(new HttpHost(url.getHost(), url.getPort()), requestOut);
		}
		else
		{
			response = httpClient.execute(new HttpHost(url.getHost()), requestOut);
		}

		T responseObj = new Gson().fromJson(
				EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8),
				(Type) responseSample.getClass());

		httpClient.close();
		LOG.info("end forward, uri:" + request.getRequestURI());

		return responseObj;
	}


	public OpenApiResponse forward(HttpServletRequest request) throws Exception
	{
		LOG.info("start forward, uri:" + request.getRequestURI());
		String method = request.getMethod();
		String urlStr;
		String uri = request.getRequestURI();
		Pattern pattern = Pattern.compile("oapi/api/(.*)");
		Matcher matcher = pattern.matcher(uri);
		if (matcher.find())
		{
			urlStr = mmsUrl + "/mmsAdmin/oapi/api/" + matcher.group(1);
		}
		else
		{
			String errorMsg = "uri error, uri:" + uri;
			LOG.error(errorMsg);
			throw new Exception(errorMsg);
		}

		OapiHttpRequestWrapper wrapper = new OapiHttpRequestWrapper(request);
		String body = wrapper.getBodyInStringFormat();
		URL url = new URL(urlStr);
		HttpRequest requestOut = new HttpRequestBase(url.toURI(), body, method);

		LOG.info("set header");
		Enumeration<String> headerNames = request.getHeaderNames();
		if (headerNames != null)
		{
			while (headerNames.hasMoreElements())
			{
				String key = headerNames.nextElement();
				String value = request.getHeader(key);
				if (!headerBan.contains(key))
				{
					requestOut.addHeader(key, value);
				}
			}
		}

		LOG.info("send request, url:"+urlStr);
		HttpResponse response;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		if (url.getPort() != 0)
		{
			response = httpClient.execute(new HttpHost(url.getHost(), url.getPort()), requestOut);
		}
		else
		{
			response = httpClient.execute(new HttpHost(url.getHost()), requestOut);
		}

		if (response.getStatusLine().getStatusCode() != 200)
		{
			String errorMsg = "request error" +
					", code:" + response.getStatusLine().getStatusCode() +
					", reason:" + response.getStatusLine().getReasonPhrase();
			LOG.error(errorMsg);
			throw new Exception(errorMsg);
		}

		OpenApiResponse responseObj = new Gson().fromJson(
				EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8),
				OpenApiResponse.class);
		httpClient.close();
		LOG.info("end forward, uri:" + request.getRequestURI());

		return responseObj;
	}

	public void setOapiResponse(HttpServletResponse response, OpenApiResponse responseObj) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(responseObj);
		response.getWriter().write(jsonString);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
	}
}