package com.oapi.model.dto.oapi;


public class OverseaDeliveryDetails
{
	private String overseaWaybillNumber;

	private String overseaDispatchDate;
	private String overseaCarrier;

	public String getOverseaWaybillNumber()
	{
		return overseaWaybillNumber;
	}

	public void setOverseaWaybillNumber(String overseaWaybillNumber)
	{
		this.overseaWaybillNumber = overseaWaybillNumber;
	}

	public String getOverseaDispatchDate()
	{
		return overseaDispatchDate;
	}

	public void setOverseaDispatchDate(String overseaDispatchDate)
	{
		this.overseaDispatchDate = overseaDispatchDate;
	}

	public String getOverseaCarrier()
	{
		return overseaCarrier;
	}

	public void setOverseaCarrier(String overseaCarrier)
	{
		this.overseaCarrier = overseaCarrier;
	}
}