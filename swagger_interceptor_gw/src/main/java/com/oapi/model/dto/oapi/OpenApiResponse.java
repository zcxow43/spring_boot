package com.oapi.model.dto.oapi;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.Collection;


public class OpenApiResponse<T> implements IMessage
{
	@JsonIgnore
	private Integer pageSize = 20;

	public enum Code
	{
		success,
		error
	}

	public enum Status
	{
		success,
		failed
	}

	private String message;

	private Code code;

	private T data;


	private Pagination pagination;

	@Override
	public String getMessage()
	{
		return message;
	}

	@Override
	public void setMessage(String message)
	{
		this.message = message;
	}

	public Code getCode()
	{
		return code;
	}

	public void setCode(Code code)
	{
		this.code = code;
	}

	public T getData()
	{
		return data;
	}

	public void setData(T data)
	{
		Integer sum = 0;
		if (data instanceof Collection)
		{
			sum = new ArrayList<>((Collection<?>) data).size();
		}
		else if(data != null)
		{
			sum = 1;
		}

		this.data = data;
		this.pagination = new Pagination();
		this.pagination.setPageSize(pageSize);
		this.pagination.setPage((int) Math.ceil((float) sum / pageSize));
		this.pagination.setTotal(sum);
	}

	@JsonIgnore
	public Integer getPageSize()
	{
		return pageSize;
	}

	public Pagination getPagination()
	{
		return pagination;
	}

	public void setPagination(Pagination pagination)
	{
		this.pagination = pagination;
	}
}