package com.oapi.model.dto.oapi;

public interface IMessage
{
	String getMessage();

	void setMessage(String message);
}