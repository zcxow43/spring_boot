package com.oapi.model.dto.oapi;

import com.oapi.model.dto.oapi.OpenApiResponse.Status;


public class UpdateOrderStatusErrorResponse implements IMessage
{
	private Status status;
	private String trackingId;
	private String message;

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	public String getTrackingId()
	{
		return trackingId;
	}

	public void setTrackingId(String trackingId)
	{
		this.trackingId = trackingId;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}
}