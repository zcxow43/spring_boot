package com.oapi.model.dto.oapi;

import com.oapi.model.dto.oapi.OpenApiResponse.Status;



public class UpdateProductResponse implements IMessage
{
	private String platformCode;
	private String skuCode;
	private Status status;
	private String message;

	public String getPlatformCode()
	{
		return platformCode;
	}

	public void setPlatformCode(String platformCode)
	{
		this.platformCode = platformCode;
	}

	public String getSkuCode()
	{
		return skuCode;
	}

	public void setSkuCode(String skuCode)
	{
		this.skuCode = skuCode;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	@Override
	public String getMessage()
	{
		return message;
	}

	@Override
	public void setMessage(String message)
	{
		this.message = message;
	}
}