package com.oapi.model.dto.oapi;

import java.util.List;


public class OrdersResponse
{
	private List<String> subOrderNumbers;

	public List<String> getSubOrderNumbers()
	{
		return subOrderNumbers;
	}

	public void setSubOrderNumbers(List<String> subOrderNumbers)
	{
		this.subOrderNumbers = subOrderNumbers;
	}
}