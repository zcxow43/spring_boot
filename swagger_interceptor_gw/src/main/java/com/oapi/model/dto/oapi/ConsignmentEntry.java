package com.oapi.model.dto.oapi;

public class ConsignmentEntry
{
	private String skuId;
	private String skuNameEn;
	private String skuNameZh;
	private String brand;
	private int quantity;

	public String getSkuId()
	{
		return skuId;
	}

	public void setSkuId(String skuId)
	{
		this.skuId = skuId;
	}

	public String getSkuNameEn()
	{
		return skuNameEn;
	}

	public void setSkuNameEn(String skuNameEn)
	{
		this.skuNameEn = skuNameEn;
	}

	public String getSkuNameZh()
	{
		return skuNameZh;
	}

	public void setSkuNameZh(String skuNameZh)
	{
		this.skuNameZh = skuNameZh;
	}

	public String getBrand()
	{
		return brand;
	}

	public void setBrand(String brand)
	{
		this.brand = brand;
	}

	public int getQuantity()
	{
		return quantity;
	}

	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}
}