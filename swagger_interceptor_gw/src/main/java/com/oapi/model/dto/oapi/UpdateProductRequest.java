package com.oapi.model.dto.oapi;

import java.math.BigDecimal;
import java.util.List;


public class UpdateProductRequest extends DateBase
{
	private String platformCode;
	private String storefrontStoreCode;
	private String fullSkuCode;
	private String skuCode;
	private String skuName;
	private String skuNameTchi;
	private String primaryCategoryCode;
	private List<ProductCategories> productCategories;
	private String skuSDescEn;
	private String skuSDescCh;
	private String skuLDescEn;
	private String skuLDescCh;
	private String invoiceRemarksEn;
	private String invoiceRemarksCh;
	private String videoLink;
	private String videoLinkEn;
	private String videoLinkCh;
	private String barcode;
	private String brandCode;
	private String manuCountry;
	private BigDecimal weight;
	private String weightUnit;
	private BigDecimal packHeight;
	private BigDecimal packLength;
	private BigDecimal packDepth;
	private String packDimensionUnit;
	private String packBoxType;
	private String packSpecEn;
	private String packSpecCh;
	private String currencyCode;
	private BigDecimal originalPrice;
	private BigDecimal sellingPrice;
	private String productReadyMethod;
	private Integer returnDays;
	private Integer productReadyDays;
	private String pickupDays;
	private String pickupTimeslot;
	private String colorEn;
	private String sizeSystem;
	private String size;
	private String invisibleFlag;
	private String colorFamilies;
	private String featureStartTime;
	private String featureEndTime;
	private String redeemStartDate;
	private String fixedRedemptionDate;
	private Integer uponPurchaseDate;
	private String finePrintEn;
	private String finePrintCh;
	private BigDecimal cost;
	private String removalServices;
	private String field1;
	private String value1;
	private String field2;
	private String value2;
	private String field3;
	private String value3;
	private String discountText;
	private String discountTextTchi;
	private String style;
	private String goodsType;
	private String warrantyPeriodUnit;
	private Integer warrantyPeriod;
	private String warrantySupplierCh;
	private String warrantySupplierEn;
	private String serviceCentreAddressCh;
	private String serviceCentreAddressEn;
	private String serviceCentreEmail;
	private String serviceCentreContact;
	private String warrantyRemarkEn;
	private String warrantyRemarkCh;
	private String onOfflineStatus;

	public String getPlatformCode()
	{
		return platformCode;
	}

	public void setPlatformCode(String platformCode)
	{
		this.platformCode = platformCode;
	}

	public String getStorefrontStoreCode()
	{
		return storefrontStoreCode;
	}

	public void setStorefrontStoreCode(String storefrontStoreCode)
	{
		this.storefrontStoreCode = storefrontStoreCode;
	}

	public String getFullSkuCode()
	{
		return fullSkuCode;
	}

	public void setFullSkuCode(String fullSkuCode)
	{
		this.fullSkuCode = fullSkuCode;
	}

	public String getSkuCode()
	{
		return skuCode;
	}

	public void setSkuCode(String skuCode)
	{
		this.skuCode = skuCode;
	}

	public String getSkuName()
	{
		return skuName;
	}

	public void setSkuName(String skuName)
	{
		this.skuName = skuName;
	}

	public String getSkuNameTchi()
	{
		return skuNameTchi;
	}

	public void setSkuNameTchi(String skuNameTchi)
	{
		this.skuNameTchi = skuNameTchi;
	}

	public String getPrimaryCategoryCode()
	{
		return primaryCategoryCode;
	}

	public void setPrimaryCategoryCode(String primaryCategoryCode)
	{
		this.primaryCategoryCode = primaryCategoryCode;
	}

	public List<ProductCategories> getProductCategories()
	{
		return productCategories;
	}

	public void setProductCategories(List<ProductCategories> productCategories)
	{
		this.productCategories = productCategories;
	}

	public String getSkuSDescEn()
	{
		return skuSDescEn;
	}

	public void setSkuSDescEn(String skuSDescEn)
	{
		this.skuSDescEn = skuSDescEn;
	}

	public String getSkuSDescCh()
	{
		return skuSDescCh;
	}

	public void setSkuSDescCh(String skuSDescCh)
	{
		this.skuSDescCh = skuSDescCh;
	}

	public String getSkuLDescEn()
	{
		return skuLDescEn;
	}

	public void setSkuLDescEn(String skuLDescEn)
	{
		this.skuLDescEn = skuLDescEn;
	}

	public String getSkuLDescCh()
	{
		return skuLDescCh;
	}

	public void setSkuLDescCh(String skuLDescCh)
	{
		this.skuLDescCh = skuLDescCh;
	}

	public String getInvoiceRemarksEn()
	{
		return invoiceRemarksEn;
	}

	public void setInvoiceRemarksEn(String invoiceRemarksEn)
	{
		this.invoiceRemarksEn = invoiceRemarksEn;
	}

	public String getInvoiceRemarksCh()
	{
		return invoiceRemarksCh;
	}

	public void setInvoiceRemarksCh(String invoiceRemarksCh)
	{
		this.invoiceRemarksCh = invoiceRemarksCh;
	}

	public String getVideoLink()
	{
		return videoLink;
	}

	public void setVideoLink(String videoLink)
	{
		this.videoLink = videoLink;
	}

	public String getVideoLinkEn()
	{
		return videoLinkEn;
	}

	public void setVideoLinkEn(String videoLinkEn)
	{
		this.videoLinkEn = videoLinkEn;
	}

	public String getVideoLinkCh()
	{
		return videoLinkCh;
	}

	public void setVideoLinkCh(String videoLinkCh)
	{
		this.videoLinkCh = videoLinkCh;
	}

	public String getBarcode()
	{
		return barcode;
	}

	public void setBarcode(String barcode)
	{
		this.barcode = barcode;
	}

	public String getBrandCode()
	{
		return brandCode;
	}

	public void setBrandCode(String brandCode)
	{
		this.brandCode = brandCode;
	}

	public String getManuCountry()
	{
		return manuCountry;
	}

	public void setManuCountry(String manuCountry)
	{
		this.manuCountry = manuCountry;
	}

	public BigDecimal getWeight()
	{
		return weight;
	}

	public void setWeight(BigDecimal weight)
	{
		this.weight = weight;
	}

	public String getWeightUnit()
	{
		return weightUnit;
	}

	public void setWeightUnit(String weightUnit)
	{
		this.weightUnit = weightUnit;
	}

	public BigDecimal getPackHeight()
	{
		return packHeight;
	}

	public void setPackHeight(BigDecimal packHeight)
	{
		this.packHeight = packHeight;
	}

	public BigDecimal getPackLength()
	{
		return packLength;
	}

	public void setPackLength(BigDecimal packLength)
	{
		this.packLength = packLength;
	}

	public BigDecimal getPackDepth()
	{
		return packDepth;
	}

	public void setPackDepth(BigDecimal packDepth)
	{
		this.packDepth = packDepth;
	}

	public String getPackDimensionUnit()
	{
		return packDimensionUnit;
	}

	public void setPackDimensionUnit(String packDimensionUnit)
	{
		this.packDimensionUnit = packDimensionUnit;
	}

	public String getPackBoxType()
	{
		return packBoxType;
	}

	public void setPackBoxType(String packBoxType)
	{
		this.packBoxType = packBoxType;
	}

	public String getPackSpecEn()
	{
		return packSpecEn;
	}

	public void setPackSpecEn(String packSpecEn)
	{
		this.packSpecEn = packSpecEn;
	}

	public String getPackSpecCh()
	{
		return packSpecCh;
	}

	public void setPackSpecCh(String packSpecCh)
	{
		this.packSpecCh = packSpecCh;
	}

	public String getCurrencyCode()
	{
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode)
	{
		this.currencyCode = currencyCode;
	}

	public BigDecimal getOriginalPrice()
	{
		return originalPrice;
	}

	public void setOriginalPrice(BigDecimal originalPrice)
	{
		this.originalPrice = originalPrice;
	}

	public BigDecimal getSellingPrice()
	{
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice)
	{
		this.sellingPrice = sellingPrice;
	}

	public String getProductReadyMethod()
	{
		return productReadyMethod;
	}

	public void setProductReadyMethod(String productReadyMethod)
	{
		this.productReadyMethod = productReadyMethod;
	}

	public Integer getReturnDays()
	{
		return returnDays;
	}

	public void setReturnDays(Integer returnDays)
	{
		this.returnDays = returnDays;
	}

	public Integer getProductReadyDays()
	{
		return productReadyDays;
	}

	public void setProductReadyDays(Integer productReadyDays)
	{
		this.productReadyDays = productReadyDays;
	}

	public String getPickupDays()
	{
		return pickupDays;
	}

	public void setPickupDays(String pickupDays)
	{
		this.pickupDays = pickupDays;
	}

	public String getPickupTimeslot()
	{
		return pickupTimeslot;
	}

	public void setPickupTimeslot(String pickupTimeslot)
	{
		this.pickupTimeslot = pickupTimeslot;
	}

	public String getColorEn()
	{
		return colorEn;
	}

	public void setColorEn(String colorEn)
	{
		this.colorEn = colorEn;
	}

	public String getSizeSystem()
	{
		return sizeSystem;
	}

	public void setSizeSystem(String sizeSystem)
	{
		this.sizeSystem = sizeSystem;
	}

	public String getSize()
	{
		return size;
	}

	public void setSize(String size)
	{
		this.size = size;
	}

	public String getInvisibleFlag()
	{
		return invisibleFlag;
	}

	public void setInvisibleFlag(String invisibleFlag)
	{
		this.invisibleFlag = invisibleFlag;
	}

	public String getColorFamilies()
	{
		return colorFamilies;
	}

	public void setColorFamilies(String colorFamilies)
	{
		this.colorFamilies = colorFamilies;
	}

	public String getFeatureStartTime()
	{
		return featureStartTime;
	}

	public void setFeatureStartTime(String featureStartTime)
	{
		this.featureStartTime = featureStartTime;
	}

	public String getFeatureEndTime()
	{
		return featureEndTime;
	}

	public void setFeatureEndTime(String featureEndTime)
	{
		this.featureEndTime = featureEndTime;
	}

	public String getRedeemStartDate()
	{
		return redeemStartDate;
	}

	public void setRedeemStartDate(String redeemStartDate)
	{
		this.redeemStartDate = redeemStartDate;
	}

	public String getFixedRedemptionDate()
	{
		return fixedRedemptionDate;
	}

	public void setFixedRedemptionDate(String fixedRedemptionDate)
	{
		this.fixedRedemptionDate = fixedRedemptionDate;
	}

	public Integer getUponPurchaseDate()
	{
		return uponPurchaseDate;
	}

	public void setUponPurchaseDate(Integer uponPurchaseDate)
	{
		this.uponPurchaseDate = uponPurchaseDate;
	}

	public String getFinePrintEn()
	{
		return finePrintEn;
	}

	public void setFinePrintEn(String finePrintEn)
	{
		this.finePrintEn = finePrintEn;
	}

	public String getFinePrintCh()
	{
		return finePrintCh;
	}

	public void setFinePrintCh(String finePrintCh)
	{
		this.finePrintCh = finePrintCh;
	}

	public BigDecimal getCost()
	{
		return cost;
	}

	public void setCost(BigDecimal cost)
	{
		this.cost = cost;
	}

	public String getRemovalServices()
	{
		return removalServices;
	}

	public void setRemovalServices(String removalServices)
	{
		this.removalServices = removalServices;
	}

	public String getField1()
	{
		return field1;
	}

	public void setField1(String field1)
	{
		this.field1 = field1;
	}

	public String getValue1()
	{
		return value1;
	}

	public void setValue1(String value1)
	{
		this.value1 = value1;
	}

	public String getField2()
	{
		return field2;
	}

	public void setField2(String field2)
	{
		this.field2 = field2;
	}

	public String getValue2()
	{
		return value2;
	}

	public void setValue2(String value2)
	{
		this.value2 = value2;
	}

	public String getField3()
	{
		return field3;
	}

	public void setField3(String field3)
	{
		this.field3 = field3;
	}

	public String getValue3()
	{
		return value3;
	}

	public void setValue3(String value3)
	{
		this.value3 = value3;
	}

	public String getDiscountText()
	{
		return discountText;
	}

	public void setDiscountText(String discountText)
	{
		this.discountText = discountText;
	}

	public String getDiscountTextTchi()
	{
		return discountTextTchi;
	}

	public void setDiscountTextTchi(String discountTextTchi)
	{
		this.discountTextTchi = discountTextTchi;
	}

	public String getStyle()
	{
		return style;
	}

	public void setStyle(String style)
	{
		this.style = style;
	}

	public String getGoodsType()
	{
		return goodsType;
	}

	public void setGoodsType(String goodsType)
	{
		this.goodsType = goodsType;
	}

	public String getWarrantyPeriodUnit()
	{
		return warrantyPeriodUnit;
	}

	public void setWarrantyPeriodUnit(String warrantyPeriodUnit)
	{
		this.warrantyPeriodUnit = warrantyPeriodUnit;
	}

	public Integer getWarrantyPeriod()
	{
		return warrantyPeriod;
	}

	public void setWarrantyPeriod(Integer warrantyPeriod)
	{
		this.warrantyPeriod = warrantyPeriod;
	}

	public String getWarrantySupplierCh()
	{
		return warrantySupplierCh;
	}

	public void setWarrantySupplierCh(String warrantySupplierCh)
	{
		this.warrantySupplierCh = warrantySupplierCh;
	}

	public String getWarrantySupplierEn()
	{
		return warrantySupplierEn;
	}

	public void setWarrantySupplierEn(String warrantySupplierEn)
	{
		this.warrantySupplierEn = warrantySupplierEn;
	}

	public String getServiceCentreAddressCh()
	{
		return serviceCentreAddressCh;
	}

	public void setServiceCentreAddressCh(String serviceCentreAddressCh)
	{
		this.serviceCentreAddressCh = serviceCentreAddressCh;
	}

	public String getServiceCentreAddressEn()
	{
		return serviceCentreAddressEn;
	}

	public void setServiceCentreAddressEn(String serviceCentreAddressEn)
	{
		this.serviceCentreAddressEn = serviceCentreAddressEn;
	}

	public String getServiceCentreEmail()
	{
		return serviceCentreEmail;
	}

	public void setServiceCentreEmail(String serviceCentreEmail)
	{
		this.serviceCentreEmail = serviceCentreEmail;
	}

	public String getServiceCentreContact()
	{
		return serviceCentreContact;
	}

	public void setServiceCentreContact(String serviceCentreContact)
	{
		this.serviceCentreContact = serviceCentreContact;
	}

	public String getWarrantyRemarkEn()
	{
		return warrantyRemarkEn;
	}

	public void setWarrantyRemarkEn(String warrantyRemarkEn)
	{
		this.warrantyRemarkEn = warrantyRemarkEn;
	}

	public String getWarrantyRemarkCh()
	{
		return warrantyRemarkCh;
	}

	public void setWarrantyRemarkCh(String warrantyRemarkCh)
	{
		this.warrantyRemarkCh = warrantyRemarkCh;
	}

	public String getOnOfflineStatus()
	{
		return onOfflineStatus;
	}

	public void setOnOfflineStatus(String onOfflineStatus)
	{
		this.onOfflineStatus = onOfflineStatus;
	}
}