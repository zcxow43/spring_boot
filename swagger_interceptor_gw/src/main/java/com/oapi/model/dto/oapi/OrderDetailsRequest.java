package com.oapi.model.dto.oapi;

public class OrderDetailsRequest
{
	private String subOrderNumber;

	public String getSubOrderNumber()
	{
		return subOrderNumber;
	}

	public void setSubOrderNumber(String subOrderNumber)
	{
		this.subOrderNumber = subOrderNumber;
	}
}