package com.oapi.model.dto.oapi;

import com.fasterxml.jackson.annotation.JsonIgnore;


public class ProductCategories
{
	private Integer categoryId;
	private Integer busUnitId;
	private String name;
	private String productCatCode;
	private String productCatName;

	@JsonIgnore
	public Integer getCategoryId()
	{
		return categoryId;
	}

	public void setCategoryId(Integer categoryId)
	{
		this.categoryId = categoryId;
	}

	@JsonIgnore
	public Integer getBusUnitId()
	{
		return busUnitId;
	}

	public void setBusUnitId(Integer busUnitId)
	{
		this.busUnitId = busUnitId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getProductCatCode()
	{
		return productCatCode;
	}

	public void setProductCatCode(String productCatCode)
	{
		this.productCatCode = productCatCode;
	}

	public String getProductCatName()
	{
		return productCatName;
	}

	public void setProductCatName(String productCatName)
	{
		this.productCatName = productCatName;
	}
}