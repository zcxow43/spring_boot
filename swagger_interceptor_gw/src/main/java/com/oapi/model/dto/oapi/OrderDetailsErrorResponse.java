package com.oapi.model.dto.oapi;

import com.oapi.model.dto.oapi.OpenApiResponse.Status;


public class OrderDetailsErrorResponse implements IMessage
{
	private String subOrderNumber;
	private Status status;
	private String message;

	public String getSubOrderNumber()
	{
		return subOrderNumber;
	}

	public void setSubOrderNumber(String subOrderNumber)
	{
		this.subOrderNumber = subOrderNumber;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	@Override
	public String getMessage()
	{
		return message;
	}

	@Override
	public void setMessage(String message)
	{
		this.message = message;
	}
}