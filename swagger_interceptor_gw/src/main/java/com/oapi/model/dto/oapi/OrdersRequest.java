package com.oapi.model.dto.oapi;

public class OrdersRequest
{
	private String orderDateStart;
	private String orderDateEnd;
	private String pickupDateStart;
	private String pickupDateEnd;

	public String getOrderDateStart()
	{
		return orderDateStart;
	}

	public void setOrderDateStart(String orderDateStart)
	{
		this.orderDateStart = orderDateStart;
	}

	public String getOrderDateEnd()
	{
		return orderDateEnd;
	}

	public void setOrderDateEnd(String orderDateEnd)
	{
		this.orderDateEnd = orderDateEnd;
	}

	public String getPickupDateStart()
	{
		return pickupDateStart;
	}

	public void setPickupDateStart(String pickupDateStart)
	{
		this.pickupDateStart = pickupDateStart;
	}

	public String getPickupDateEnd()
	{
		return pickupDateEnd;
	}

	public void setPickupDateEnd(String pickupDateEnd)
	{
		this.pickupDateEnd = pickupDateEnd;
	}
}