package com.oapi.model.dto.oapi;



public class MerchantDeliveryDetails
{
	private String deliveryMethod;

	private String deliveryDate;
	private String referenceNumber;

	private String dateContactCustomer;

	private String remark;

	public String getDeliveryMethod()
	{
		return deliveryMethod;
	}

	public void setDeliveryMethod(String deliveryMethod)
	{
		this.deliveryMethod = deliveryMethod;
	}

	public String getDeliveryDate()
	{
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate)
	{
		this.deliveryDate = deliveryDate;
	}

	public String getReferenceNumber()
	{
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber)
	{
		this.referenceNumber = referenceNumber;
	}

	public String getDateContactCustomer()
	{
		return dateContactCustomer;
	}

	public void setDateContactCustomer(String dateContactCustomer)
	{
		this.dateContactCustomer = dateContactCustomer;
	}

	public String getRemark()
	{
		return remark;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}
}