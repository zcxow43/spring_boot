package com.oapi.model.dto.oapi;

import java.math.BigDecimal;


public class GetStoreResponse
{
	private String platformCode;
	private String storefrontStoreCode;
	private String storeNameEn;
	private String storeNameZh;
	private String merchantNameEn;
	private String merchantNameZh;
	private BigDecimal deliveryThreshold;
	private BigDecimal deliveryFee;

	public String getPlatformCode()
	{
		return platformCode;
	}

	public void setPlatformCode(String platformCode)
	{
		this.platformCode = platformCode;
	}

	public String getStorefrontStoreCode()
	{
		return storefrontStoreCode;
	}

	public void setStorefrontStoreCode(String storefrontStoreCode)
	{
		this.storefrontStoreCode = storefrontStoreCode;
	}

	public String getStoreNameEn()
	{
		return storeNameEn;
	}

	public void setStoreNameEn(String storeNameEn)
	{
		this.storeNameEn = storeNameEn;
	}

	public String getStoreNameZh()
	{
		return storeNameZh;
	}

	public void setStoreNameZh(String storeNameZh)
	{
		this.storeNameZh = storeNameZh;
	}

	public String getMerchantNameEn()
	{
		return merchantNameEn;
	}

	public void setMerchantNameEn(String merchantNameEn)
	{
		this.merchantNameEn = merchantNameEn;
	}

	public String getMerchantNameZh()
	{
		return merchantNameZh;
	}

	public void setMerchantNameZh(String merchantNameZh)
	{
		this.merchantNameZh = merchantNameZh;
	}

	public BigDecimal getDeliveryThreshold()
	{
		return deliveryThreshold;
	}

	public void setDeliveryThreshold(BigDecimal deliveryThreshold)
	{
		this.deliveryThreshold = deliveryThreshold;
	}

	public BigDecimal getDeliveryFee()
	{
		return deliveryFee;
	}

	public void setDeliveryFee(BigDecimal deliveryFee)
	{
		this.deliveryFee = deliveryFee;
	}
}