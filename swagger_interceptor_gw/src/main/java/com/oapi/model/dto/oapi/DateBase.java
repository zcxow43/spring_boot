package com.oapi.model.dto.oapi;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


public class DateBase
{
	private String createdBy;
	@JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
	private Date createdDate;
	private String lastUpdatedBy;
	@JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
	private Date lastUpdatedDate;

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	public Date getCreatedDate()
	{
		return createdDate;
	}

	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}

	public String getLastUpdatedBy()
	{
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy)
	{
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate()
	{
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate)
	{
		this.lastUpdatedDate = lastUpdatedDate;
	}
}