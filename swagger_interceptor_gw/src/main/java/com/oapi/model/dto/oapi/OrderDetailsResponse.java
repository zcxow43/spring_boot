package com.oapi.model.dto.oapi;

import java.util.List;


public class OrderDetailsResponse
{
	private String consignmentCode;
	private String consignmentStatus;
	private String orderId;
	private String orderDate;
	private String trackingId;
	private String transactionId;
	private String pickUpDate;
	private String deliveryDate;
	private List<ConsignmentEntry> consignmentEntries;

	public String getConsignmentCode()
	{
		return consignmentCode;
	}

	public void setConsignmentCode(String consignmentCode)
	{
		this.consignmentCode = consignmentCode;
	}

	public String getConsignmentStatus()
	{
		return consignmentStatus;
	}

	public void setConsignmentStatus(String consignmentStatus)
	{
		this.consignmentStatus = consignmentStatus;
	}

	public String getOrderId()
	{
		return orderId;
	}

	public void setOrderId(String orderId)
	{
		this.orderId = orderId;
	}

	public String getOrderDate()
	{
		return orderDate;
	}

	public void setOrderDate(String orderDate)
	{
		this.orderDate = orderDate;
	}

	public String getTrackingId()
	{
		return trackingId;
	}

	public void setTrackingId(String trackingId)
	{
		this.trackingId = trackingId;
	}

	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(String transactionId)
	{
		this.transactionId = transactionId;
	}

	public String getPickUpDate()
	{
		return pickUpDate;
	}

	public void setPickUpDate(String pickUpDate)
	{
		this.pickUpDate = pickUpDate;
	}

	public String getDeliveryDate()
	{
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate)
	{
		this.deliveryDate = deliveryDate;
	}

	public List<ConsignmentEntry> getConsignmentEntries()
	{
		return consignmentEntries;
	}

	public void setConsignmentEntries(List<ConsignmentEntry> consignmentEntries)
	{
		this.consignmentEntries = consignmentEntries;
	}
}