package com.oapi.model.dto.oapi;

public class GetStockRequest
{
	private String productId;
	private String warehouseId;

	public String getProductId()
	{
		return productId;
	}

	public void setProductId(String productId)
	{
		this.productId = productId;
	}

	public String getWarehouseId()
	{
		return warehouseId;
	}

	public void setWarehouseId(String warehouseId)
	{
		this.warehouseId = warehouseId;
	}
}