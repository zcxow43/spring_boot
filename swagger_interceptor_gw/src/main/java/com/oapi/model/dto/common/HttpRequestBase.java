package com.oapi.model.dto.common;

import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.entity.StringEntity;

import java.io.UnsupportedEncodingException;
import java.net.URI;


public class HttpRequestBase extends HttpEntityEnclosingRequestBase
{
	private String method;

	public HttpRequestBase(URI uri, String requestBody, String method) throws UnsupportedEncodingException
	{
		this.setURI(uri);
		super.setEntity(new StringEntity(requestBody));
		this.method = method;
	}

	@Override
	public String getMethod()
	{
		return method;
	}
}