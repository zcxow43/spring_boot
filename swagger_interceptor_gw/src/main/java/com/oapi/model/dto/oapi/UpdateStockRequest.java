package com.oapi.model.dto.oapi;

public class UpdateStockRequest
{
	private String productId;
	private String warehouseId;
	private Integer quantity;
	private String action;

	public String getProductId()
	{
		return productId;
	}

	public void setProductId(String productId)
	{
		this.productId = productId;
	}

	public String getWarehouseId()
	{
		return warehouseId;
	}

	public void setWarehouseId(String warehouseId)
	{
		this.warehouseId = warehouseId;
	}

	public Integer getQuantity()
	{
		return quantity;
	}

	public void setQuantity(Integer quantity)
	{
		this.quantity = quantity;
	}

	public String getAction()
	{
		return action;
	}

	public void setAction(String action)
	{
		this.action = action;
	}
}