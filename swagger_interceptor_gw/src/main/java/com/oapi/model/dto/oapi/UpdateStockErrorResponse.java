package com.oapi.model.dto.oapi;

import com.oapi.model.dto.oapi.OpenApiResponse.Status;


public class UpdateStockErrorResponse implements IMessage
{
	private String productId;
	private Status status;
	private String message;

	public String getProductId()
	{
		return productId;
	}

	public void setProductId(String productId)
	{
		this.productId = productId;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	@Override
	public String getMessage()
	{
		return message;
	}

	@Override
	public void setMessage(String message)
	{
		this.message = message;
	}
}