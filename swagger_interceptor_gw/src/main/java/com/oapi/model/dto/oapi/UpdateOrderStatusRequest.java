package com.oapi.model.dto.oapi;

public class UpdateOrderStatusRequest
{
	private String subOrderNumber;
	private String status;
	private String trackingId;
	private MerchantDeliveryDetails merchantDeliveryDetails;
	private OverseaDeliveryDetails overseaDeliveryDetails;

	public String getSubOrderNumber()
	{
		return subOrderNumber;
	}

	public void setSubOrderNumber(String subOrderNumber)
	{
		this.subOrderNumber = subOrderNumber;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getTrackingId()
	{
		return trackingId;
	}

	public void setTrackingId(String trackingId)
	{
		this.trackingId = trackingId;
	}

	public MerchantDeliveryDetails getMerchantDeliveryDetails()
	{
		return merchantDeliveryDetails;
	}

	public void setMerchantDeliveryDetails(MerchantDeliveryDetails merchantDeliveryDetails)
	{
		this.merchantDeliveryDetails = merchantDeliveryDetails;
	}

	public OverseaDeliveryDetails getOverseaDeliveryDetails()
	{
		return overseaDeliveryDetails;
	}

	public void setOverseaDeliveryDetails(OverseaDeliveryDetails overseaDeliveryDetails)
	{
		this.overseaDeliveryDetails = overseaDeliveryDetails;
	}
}