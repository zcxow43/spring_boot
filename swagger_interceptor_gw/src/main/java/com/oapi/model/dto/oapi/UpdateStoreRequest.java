package com.oapi.model.dto.oapi;

import java.math.BigDecimal;
import java.util.Date;


public class UpdateStoreRequest
{
	private Integer id;
	private String platformCode;
	private String storefrontStoreCode;
	private String storeNameEn;
	private String storeNameZh;
	private BigDecimal deliveryThreshold;
	private BigDecimal deliveryFee;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getPlatformCode()
	{
		return platformCode;
	}

	public void setPlatformCode(String platformCode)
	{
		this.platformCode = platformCode;
	}

	public String getStorefrontStoreCode()
	{
		return storefrontStoreCode;
	}

	public void setStorefrontStoreCode(String storefrontStoreCode)
	{
		this.storefrontStoreCode = storefrontStoreCode;
	}

	public String getStoreNameEn()
	{
		return storeNameEn;
	}

	public void setStoreNameEn(String storeNameEn)
	{
		this.storeNameEn = storeNameEn;
	}

	public String getStoreNameZh()
	{
		return storeNameZh;
	}

	public void setStoreNameZh(String storeNameZh)
	{
		this.storeNameZh = storeNameZh;
	}

	public BigDecimal getDeliveryThreshold()
	{
		return deliveryThreshold;
	}

	public void setDeliveryThreshold(BigDecimal deliveryThreshold)
	{
		this.deliveryThreshold = deliveryThreshold;
	}

	public BigDecimal getDeliveryFee()
	{
		return deliveryFee;
	}

	public void setDeliveryFee(BigDecimal deliveryFee)
	{
		this.deliveryFee = deliveryFee;
	}

	public String getLastUpdatedBy()
	{
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy)
	{
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate()
	{
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate)
	{
		this.lastUpdatedDate = lastUpdatedDate;
	}
}