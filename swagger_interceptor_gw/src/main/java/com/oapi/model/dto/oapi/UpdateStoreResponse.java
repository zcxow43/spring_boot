package com.oapi.model.dto.oapi;

import com.oapi.model.dto.oapi.OpenApiResponse.Status;



public class UpdateStoreResponse implements IMessage
{
	private String platformCode;
	private String storefrontStoreCode;
	private Status status;
	private String message;

	public String getPlatformCode()
	{
		return platformCode;
	}

	public void setPlatformCode(String platformCode)
	{
		this.platformCode = platformCode;
	}

	public String getStorefrontStoreCode()
	{
		return storefrontStoreCode;
	}

	public void setStorefrontStoreCode(String storefrontStoreCode)
	{
		this.storefrontStoreCode = storefrontStoreCode;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	@Override
	public String getMessage()
	{
		return message;
	}

	@Override
	public void setMessage(String message)
	{
		this.message = message;
	}
}