package com.oapi.controller;

import com.oapi.model.dto.oapi.GetProductRequest;
import com.oapi.model.dto.oapi.GetStockRequest;
import com.oapi.model.dto.oapi.GetStoreResponse;
import com.oapi.model.dto.oapi.OpenApiResponse;
import com.oapi.model.dto.oapi.OpenApiResponse.Code;
import com.oapi.model.dto.oapi.OrderDetailsRequest;
import com.oapi.model.dto.oapi.OrdersRequest;
import com.oapi.model.dto.oapi.OrdersResponse;
import com.oapi.model.dto.oapi.UpdateOrderStatusRequest;
import com.oapi.model.dto.oapi.UpdateProductRequest;
import com.oapi.model.dto.oapi.UpdateProductResponse;
import com.oapi.model.dto.oapi.UpdateStockRequest;
import com.oapi.model.dto.oapi.UpdateStoreRequest;
import com.oapi.model.dto.oapi.UpdateStoreResponse;
import com.oapi.service.oapi.OapiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;



import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


@Controller
//@RequestMapping(value = "mmsAdmin/oapi/api")
@RequestMapping(value = "mock")
public class OapiController
{
	protected final Logger LOG = LoggerFactory.getLogger(getClass());

	@Autowired
	private HttpServletRequest request;

	@Resource
	OapiService httpService;

	@RequestMapping(value = "/product/details", method = { RequestMethod.GET })
	@ResponseBody
	public OpenApiResponse<List<Object>> getProductDetail(@RequestHeader(value = "x-auth-token") final String token,
			@RequestHeader(value = "storeCode") final String storefrontStoreCode,
			@RequestHeader(value = "platformCode") final String platformCode,
			@RequestHeader(value = "businessType") final String businessType,
			@RequestBody List<GetProductRequest> productList)
	{
		OpenApiResponse<List<Object>> response = new OpenApiResponse();
		try
		{
			response =  httpService.forward(request, response);
		}
		catch (Exception ex)
		{
			LOG.error("mms_oapi getProductDetail error:" + ex.getMessage(), ex);
			response.setCode(Code.error);
			response.setMessage(ex.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/product", method = { RequestMethod.POST }, produces = { "application/json" })
	@ResponseBody
	public OpenApiResponse<List<UpdateProductResponse>> updateProduct(@RequestHeader(value = "x-auth-token") final String token,
			@RequestHeader(value = "storeCode") final String storefrontStoreCode,
			@RequestHeader(value = "platformCode") final String platformCode,
			@RequestHeader(value = "businessType") final String businessType,
			@RequestBody List<UpdateProductRequest> productList)
	{
		OpenApiResponse<List<UpdateProductResponse>> response = new OpenApiResponse();

		try
		{
			response = httpService.forward(request, response);
		}
		catch (Exception ex)
		{
			LOG.error("mms_oapi updateProduct error:" + ex.getMessage(), ex);
			response.setCode(OpenApiResponse.Code.error);
			response.setMessage(ex.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/store/details", method = { RequestMethod.GET })
	@ResponseBody
	public OpenApiResponse<List<GetStoreResponse>> getStoreDetail(
			@RequestHeader(value = "x-auth-token") final String token,
			@RequestHeader(value = "storeCode") final String storefrontStoreCode,
			@RequestHeader(value = "platformCode") final String platformCode,
			@RequestHeader(value = "businessType") final String businessType)
	{
		OpenApiResponse<List<GetStoreResponse>> response = new OpenApiResponse();

		try
		{
			response = httpService.forward(request, response);
		}
		catch (Exception ex)
		{
			LOG.error("mms_oapi getStoreDetail error:" + ex.getMessage(), ex);
			response.setCode(OpenApiResponse.Code.error);
			response.setMessage(ex.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/store", method = { RequestMethod.POST }, produces = { "application/json" })
	@ResponseBody
	public OpenApiResponse<List<UpdateStoreResponse>> updateStore(
			@RequestHeader(value = "x-auth-token") final String token,
			@RequestHeader(value = "storeCode") final String storefrontStoreCode,
			@RequestHeader(value = "platformCode") final String platformCode,
			@RequestHeader(value = "businessType") final String businessType,
			@RequestBody List<UpdateStoreRequest> storeList)
	{
		OpenApiResponse<List<UpdateStoreResponse>> response = new OpenApiResponse();

		try
		{
			response = httpService.forward(request, response);
		}
		catch (Exception ex)
		{
			LOG.error("mms_oapi updateStore error:" + ex.getMessage(), ex);
			response.setCode(OpenApiResponse.Code.error);
			response.setMessage(ex.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/stock/details", method = { RequestMethod.GET })
	@ResponseBody
	public OpenApiResponse<List<Object>> getStockDetails(
			@RequestHeader(value = "x-auth-token") final String token,
			@RequestHeader(value = "storeCode") final String storefrontStoreCode,
			@RequestHeader(value = "platformCode") final String platformCode,
			@RequestHeader(value = "businessType") final String businessType,
			@RequestBody List<GetStockRequest> stockList)
	{
		OpenApiResponse<List<Object>> response = new OpenApiResponse<>();

		try
		{
			response = httpService.forward(request, response);
		}
		catch (Exception ex)
		{
			LOG.error("mms_oapi getStockDetails error:" + ex.getMessage(), ex);
			response.setCode(Code.error);
			response.setMessage(ex.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/stock", method = { RequestMethod.POST })
	@ResponseBody
	public OpenApiResponse<List<Object>> updateStock(
			@RequestHeader(value = "x-auth-token") final String token,
			@RequestHeader(value = "storeCode") final String storefrontStoreCode,
			@RequestHeader(value = "platformCode") final String platformCode,
			@RequestHeader(value = "businessType") final String businessType,
			@RequestBody List<UpdateStockRequest> stockList)
	{
		OpenApiResponse<List<Object>> response = new OpenApiResponse<>();

		try
		{
			response = httpService.forward(request, response);
		}
		catch (Exception ex)
		{
			LOG.error("mms_oapi updateStock error:" + ex.getMessage(), ex);
			response.setCode(Code.error);
			response.setMessage(ex.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/order/orders", method = { RequestMethod.GET })
	@ResponseBody
	public OpenApiResponse<OrdersResponse> getOrders(@RequestHeader(value = "x-auth-token") final String token,
			@RequestHeader(value = "storeCode") final String storefrontStoreCode,
			@RequestHeader(value = "platformCode") final String platformCode,
			@RequestHeader(value = "businessType") final String businessType,
			@RequestBody OrdersRequest ordersRequest)
	{
		OpenApiResponse<OrdersResponse> response = new OpenApiResponse<>();

		try
		{
			response = httpService.forward(request, response);
		}
		catch (Exception ex)
		{
			LOG.error("mms_oapi getOrders error:" + ex.getMessage(), ex);
			response.setCode(Code.error);
			response.setMessage(ex.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/order/details", method = { RequestMethod.GET })
	@ResponseBody
	public OpenApiResponse<List<Object>> getOrderDetails(@RequestHeader(value = "x-auth-token") final String token,
			@RequestHeader(value = "storeCode") final String storefrontStoreCode,
			@RequestHeader(value = "platformCode") final String platformCode,
			@RequestHeader(value = "businessType") final String businessType,
			@RequestBody List<OrderDetailsRequest> orderDetailsRequests)
	{
		OpenApiResponse<List<Object>> response = new OpenApiResponse<>();

		try
		{
			response = httpService.forward(request, response);
		}
		catch (Exception ex)
		{
			LOG.error("mms_oapi getOrderDetails error:" + ex.getMessage(), ex);
			response.setCode(Code.error);
			response.setMessage(ex.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/order/status/update", method = { RequestMethod.POST })
	@ResponseBody
	public OpenApiResponse<List<Object>> updateOrderStatus(@RequestHeader(value = "x-auth-token") final String token,
			@RequestHeader(value = "storeCode") final String storefrontStoreCode,
			@RequestHeader(value = "platformCode") final String platformCode,
			@RequestHeader(value = "businessType") final String businessType,
			@RequestBody List<UpdateOrderStatusRequest> updateOrderStatusRequests)
	{
		OpenApiResponse<List<Object>> response = new OpenApiResponse<>();
		try
		{
			response = httpService.forward(request, response);
		}
		catch (Exception ex)
		{
			LOG.error("mms_oapi updateOrderStatus:" + ex.getMessage(), ex);
			response.setCode(Code.error);
			response.setMessage(ex.getMessage());
		}

		return response;
	}

	@RequestMapping(value = "/aaa", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public OpenApiResponse<List<Object>> aaa(@RequestHeader(value = "x-auth-token") final String token,
			@RequestHeader(value = "storeCode") final String storefrontStoreCode,
			@RequestHeader(value = "platformCode") final String platformCode,
			@RequestHeader(value = "businessType") final String businessType,
			@RequestBody List<GetProductRequest> productList) throws IOException, URISyntaxException
	{
		OpenApiResponse<List<Object>> response = new OpenApiResponse();

		return response;
	}
}