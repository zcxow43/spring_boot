package com.example.quartz.schedule;

import com.example.quartz.service.TestService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;


public class TestJob implements Job
{

	@Autowired
	TestService testService;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException
	{
		System.out.println("execute");
		testService.print();
	}
}