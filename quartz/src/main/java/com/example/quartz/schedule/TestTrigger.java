package com.example.quartz.schedule;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;


@Configuration
@EnableAutoConfiguration
public class TestTrigger
{
	@Bean
	public JobDetailFactoryBean updateUserActiveJob() {
		JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
		jobDetailFactory.setJobClass(TestJob.class);
		jobDetailFactory.setDescription("Update User Active Schedule");
		jobDetailFactory.setDurability(true);
		return jobDetailFactory;
	}

	@Bean
	public Trigger trigger(JobDetail updateUserActiveJob) {
		return TriggerBuilder.newTrigger()
				.forJob(updateUserActiveJob)
				.withIdentity("UpdateUserActive")
				.withSchedule(CronScheduleBuilder.cronSchedule("*/5 * * * * ?")).build();

//		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
//		if (scheduler.checkExists(triggerKey)) {
//			scheduler.unscheduleJob(triggerKey);
//		}
	}
}